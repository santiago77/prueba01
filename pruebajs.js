// let numero = 1925;
// console.log(typeof numero);



// *******************************************************************************

// sirve para seleccionar un elemento del html. de esta forma se selecciona un objeto entero:

//      Ejemplo:

// saludo = document.getElementById("saludo");
// document.write(saludo);

//     Mas Ejemplos y formas de seleccionar:
//                  esto devuelve una coleccion de elementos.   
// saludo = document.getElementsByTagName("p");
// document.write(saludo);

//     otra forma de seleccionar.      
//                devuelve el primer elemento que coincida con el grupo especificado 
//              de selectores.
// parrafo = document.querySelector(".parrafo");
// document.write(parrafo)

// parrafo = document.querySelectorAll(".parrafo");
// document.write(parrafo)





//          Atributos de inputs:

// const input = document.querySelector(".input-normal");
// document.write(input.className); te devuelve el nombre de la clase
// document.write(input.value); te devuelve el valor
// document.write(input.type = "number"); cambia de tipo
// input.minLength = "4"; tiene que tener como minimo 4 caracteres

// ****************************************************************************************

//                 Dorian design (video 24)



// document.getElementById('id') - Acceder a un elemento a través de su id
// document | element .querySelector('selectorCSS') - Accede al primer elemento que coincida con el selector CSS
// document | element .querySelectorAll('selectorCSS') - Accede a todos los elementos que coincidan con el selector CSS, devuelve un nodeList


// const title = document.getElementById('title')
// title.textContent = 'Hola wey!!!' con esta porpiedad se puede leer y escribir elelemento title

// const paragraph = document.querySelector('.paragraph');
// const span = paragraph.querySelector("span"); // no se le pone un punto porque directamente accede al elemento span
// console.log(span.textContent = "    Hola Soy El Span     ");
// const span = document.getElementById('title').querySelector("span") se va accediendo al elemento (del parrafo va entrando y selecciona el span)

const paragraphs = document.querySelectorAll('.paragraph')
console.log(paragraphs);
// const paragraphsSpread = [...document.querySelectorAll('.paragraph')]

// const paragraphsArray = Array.from(document.querySelectorAll('.paragraph'))

// paragraphs[0].style.color = 'red'

// paragraphs.map(p => p.style.color = 'green')

// paragraphsSpread.map(p => p.style.color = 'green')

// paragraphsArray.map(p=>p.style.color='blue')





